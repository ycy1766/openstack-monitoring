#!/bin/bash


#ansible-playbook  playbooks/50_pip_venv_install.yml

ansible-playbook  playbooks/11_zabbix_agent_install.yml \
-e ansible_python_interpreter="{{ PIP_VIRTUALENV_DIRECTORY }}/bin/python2.7"
